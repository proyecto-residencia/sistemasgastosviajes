﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data.SqlClient;
using System.Data;

namespace backendDatos
{
   public class cr_solictarviaje
    {
        public List<solicitarviaje> Listar()
        {
            List<solicitarviaje> lista = new List<solicitarviaje>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    StringBuilder sw = new StringBuilder();
                    sw.AppendLine("select cv.idsolicitud, cv.nombreviatico, cu.id,cu.nombre, cp.nombre[nombreproyecto],cv.fechasalida,cv.fechacaptura,cv.fecharegreso,ca.nombre[nombreadmin],");
                    sw.AppendLine("cc.nombre[nombrecentro],cv.comentarios,cv.origen,cv.destino,cv.estatus,cv.importe from usuarios cu inner join solicitar_viaticos cv");
                    sw.AppendLine("on cu.id=cv.claveusuario  inner join proyecto cp on cp.id=cv.claveproyecto inner join uniAdmin ca on ca.id=cv.claveuniadmin");
                    sw.AppendLine("inner join centrodecostos cc on cc.id=cv.clavecentrocostos where cv.eliminar='alta';");
                    SqlCommand cmd = new SqlCommand(sw.ToString(), oconexion);
                    cmd.CommandType = CommandType.Text;

                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista.Add(new solicitarviaje()
                            {
                                idsolicitud = Convert.ToInt16(dr["idsolicitud"]),
                                nombreviatico=dr["nombreviatico"].ToString(),
                                nombreusuario = new usuarios() { id = Convert.ToInt16(dr["id"]) ,nombre = dr["nombre"].ToString() },
                                nombreproyecto =new proyecto() {nombre=dr["nombreproyecto"].ToString() },
                                fechasalida = dr["fechasalida"].ToString(),
                                fechacaptura = dr["fechacaptura"].ToString(),
                                fecharegreso = dr["fecharegreso"].ToString(),
                                nombreuniadmin = new uniAdmin() {nombre=dr["nombreadmin"].ToString() },
                                centrocosto = new centrodecostos() {nombre=dr["nombrecentro"].ToString()},
                                comentarios = dr["comentarios"].ToString(),
                                origen = dr["origen"].ToString(),
                                destino = dr["destino"].ToString(),
                                estatus= dr["estatus"].ToString(),
                                importe = Convert.ToDecimal(dr["importe"].ToString())

                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<solicitarviaje>();

            }


            return lista;

        }


        public List<solicitarviaje> Listarviaticos(int id)
        {
            List<solicitarviaje> lista = new List<solicitarviaje>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    StringBuilder sw = new StringBuilder();
                    sw.AppendLine("select cv.idsolicitud, cv.nombreviatico, cu.id,cu.nombre, cp.nombre[nombreproyecto],cv.fechasalida,cv.fechacaptura,cv.fecharegreso,ca.nombre[nombreadmin],");
                    sw.AppendLine("cc.nombre[nombrecentro],cv.comentarios,cv.origen,cv.destino,cv.estatus,cv.importe from usuarios cu inner join solicitar_viaticos cv");
                    sw.AppendLine("on cu.id=cv.claveusuario  inner join proyecto cp on cp.id=cv.claveproyecto inner join uniAdmin ca on ca.id=cv.claveuniadmin");
                    sw.AppendLine("inner join centrodecostos cc on cc.id=cv.clavecentrocostos where cv.idsolicitud=@id");
                    SqlCommand cmd = new SqlCommand(sw.ToString(), oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;

                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista.Add(new solicitarviaje()
                            {
                                idsolicitud = Convert.ToInt16(dr["idsolicitud"]),
                                nombreviatico = dr["nombreviatico"].ToString(),
                                nombreusuario = new usuarios() { id = Convert.ToInt16(dr["id"]), nombre = dr["nombre"].ToString() },
                                nombreproyecto = new proyecto() { nombre = dr["nombreproyecto"].ToString() },
                                fechasalida = dr["fechasalida"].ToString(),
                                fechacaptura = dr["fechacaptura"].ToString(),
                                fecharegreso = dr["fecharegreso"].ToString(),
                                nombreuniadmin = new uniAdmin() { nombre = dr["nombreadmin"].ToString() },
                                centrocosto = new centrodecostos() { nombre = dr["nombrecentro"].ToString() },
                                comentarios = dr["comentarios"].ToString(),
                                origen = dr["origen"].ToString(),
                                destino = dr["destino"].ToString(),
                                estatus = dr["estatus"].ToString(),
                                importe = Convert.ToDecimal(dr["importe"].ToString())

                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<solicitarviaje>();

            }


            return lista;

        }


        public List<solicitarviaje> Listarviajesusua(int id)
        {
            List<solicitarviaje> lista = new List<solicitarviaje>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    StringBuilder sw = new StringBuilder();
                    sw.AppendLine("select cv.idsolicitud,cv.nombreviatico,cu.id,cu.nombre, cp.nombre[nombreproyecto],cv.fechasalida,cv.fechacaptura,cv.fecharegreso,ca.nombre[nombreadmin],");
                    sw.AppendLine("cc.nombre[nombrecentro],cv.comentarios,cv.origen,cv.destino,cv.estatus,cv.importe  from usuarios cu inner join solicitar_viaticos cv");
                    sw.AppendLine("on cu.id=cv.claveusuario  inner join proyecto cp on cp.id=cv.claveproyecto inner join uniAdmin ca on ca.id=cv.claveuniadmin");
                    sw.AppendLine("inner join centrodecostos cc on cc.id=cv.clavecentrocostos where cu.id=@id and cv.eliminar='alta';");
                    SqlCommand cmd = new SqlCommand(sw.ToString(), oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista.Add(new solicitarviaje()
                            {
                                idsolicitud = Convert.ToInt16(dr["idsolicitud"]),
                                nombreviatico = dr["nombreviatico"].ToString(),
                                nombreusuario = new usuarios() { id = Convert.ToInt16(dr["id"]), nombre = dr["nombre"].ToString() },
                                nombreproyecto = new proyecto() { nombre = dr["nombreproyecto"].ToString() },
                                fechasalida = dr["fechasalida"].ToString(),
                                fechacaptura = dr["fechacaptura"].ToString(),
                                fecharegreso = dr["fecharegreso"].ToString(),
                                nombreuniadmin = new uniAdmin() { nombre = dr["nombreadmin"].ToString() },
                                centrocosto = new centrodecostos() { nombre = dr["nombrecentro"].ToString() },
                                comentarios = dr["comentarios"].ToString(),
                                origen = dr["origen"].ToString(),
                                destino = dr["destino"].ToString(),
                                estatus = dr["estatus"].ToString(),
                                importe = Convert.ToDecimal(dr["importe"].ToString())

                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<solicitarviaje>();

            }


            return lista;

        }


        public List<solicitarviaje> Listarviajeporfecha(int id, string fecha1, string fecha2)
        {
            List<solicitarviaje> lista = new List<solicitarviaje>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    StringBuilder sw = new StringBuilder();
                    sw.AppendLine("select cv.idsolicitud,cv.nombreviatico,cu.id,cu.nombre, cp.nombre[nombreproyecto],cv.fechasalida,cv.fechacaptura,cv.fecharegreso,ca.nombre[nombreadmin],");
                    sw.AppendLine("cc.nombre[nombrecentro],cv.comentarios,cv.origen,cv.destino,cv.estatus,cv.importe  from usuarios cu inner join solicitar_viaticos cv");
                    sw.AppendLine("on cu.id=cv.claveusuario  inner join proyecto cp on cp.id=cv.claveproyecto inner join uniAdmin ca on ca.id=cv.claveuniadmin");
                    sw.AppendLine("inner join centrodecostos cc on cc.id=cv.clavecentrocostos where cu.id=@id and  cv.fechacaptura BETWEEN @fecha1 and @fecha2 ;");
                    SqlCommand cmd = new SqlCommand(sw.ToString(), oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@fecha1", fecha1);
                    cmd.Parameters.AddWithValue("@fecha2", fecha2);

                    cmd.CommandType = CommandType.Text;

                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista.Add(new solicitarviaje()
                            {
                                idsolicitud = Convert.ToInt16(dr["idsolicitud"]),
                                nombreviatico = dr["nombreviatico"].ToString(),
                                nombreusuario = new usuarios() { id = Convert.ToInt16(dr["id"]), nombre = dr["nombre"].ToString() },
                                nombreproyecto = new proyecto() { nombre = dr["nombreproyecto"].ToString() },
                                fechasalida = dr["fechasalida"].ToString(),
                                fechacaptura = dr["fechacaptura"].ToString(),
                                fecharegreso = dr["fecharegreso"].ToString(),
                                nombreuniadmin = new uniAdmin() { nombre = dr["nombreadmin"].ToString() },
                                centrocosto = new centrodecostos() { nombre = dr["nombrecentro"].ToString() },
                                comentarios = dr["comentarios"].ToString(),
                                origen = dr["origen"].ToString(),
                                destino = dr["destino"].ToString(),
                                estatus = dr["estatus"].ToString(),
                                importe = Convert.ToDecimal(dr["importe"].ToString())

                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<solicitarviaje>();

            }


            return lista;

        }


        public bool editarsolicitudviaje(solicitarviaje obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top(1) solicitar_viaticos set fechasalida='{0}', fechacaptura='{1}', fecharegreso='{2}',claveuniadmin='{3}',clavecentrocostos='{4}',comentarios='{5}',origen='{6}',destino='{7}' where idsolicitud =@id", obj.fechasalida, obj.fechacaptura, obj.fecharegreso,obj.nombreuniadmin.id,obj.centrocosto.id,obj.comentarios,obj.origen,obj.destino);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.idsolicitud);
                    cmd.Parameters.AddWithValue("@fechasalida", obj.fechasalida);
                    cmd.Parameters.AddWithValue("@fechacaptura", obj.fechacaptura);
                    cmd.Parameters.AddWithValue("@fecharegreso", obj.fecharegreso);
                    cmd.Parameters.AddWithValue("@nombreunidamin", obj.nombreuniadmin.id);
                    cmd.Parameters.AddWithValue("@centrocosto", obj.centrocosto.id);
                    cmd.Parameters.AddWithValue("@comentarios", obj.comentarios);
                    cmd.Parameters.AddWithValue("@origen", obj.origen);
                    cmd.Parameters.AddWithValue("@destino", obj.destino);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }


        public bool dardebaja(solicitarviaje obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top(1) solicitar_viaticos set eliminar ='eliminado' where idsolicitud=@id");
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.idsolicitud);
                  

                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }



        public bool editarimporte (solicitarviaje obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top(1) solicitar_viaticos set importe ='{0}'where idsolicitud=@id",  obj.importe);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.idsolicitud);
                    cmd.Parameters.AddWithValue("@importe", obj.importe);
                    
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }

        public bool editarviaje(solicitarviaje obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top (1) solicitar_viaticos set estatus='{0}' where idsolicitud=@id", obj.estatus);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.idsolicitud);
                    cmd.Parameters.AddWithValue("@estatus", obj.estatus);

                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }

     



        public int Registrarviaje(solicitarviaje obj, out String Mensaje)
        {
            int idautogenerado = 0;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("cp_registrarviaje", oconexion);
                    cmd.Parameters.AddWithValue("nombreviatico", obj.nombreviatico);
                    cmd.Parameters.AddWithValue("claveusuario", obj.nombreusuario.id);
                    cmd.Parameters.AddWithValue("claveproyecto", obj.nombreproyecto.id);
                    cmd.Parameters.AddWithValue("fechasalida", obj.fechasalida);
                    cmd.Parameters.AddWithValue("fechacaptura", obj.fechacaptura);
                    cmd.Parameters.AddWithValue("fecharegreso", obj.fecharegreso);
                    cmd.Parameters.AddWithValue("claveuniadmin", obj.nombreuniadmin.id);
                    cmd.Parameters.AddWithValue("clavecentrocosto", obj.centrocosto.id);
                    cmd.Parameters.AddWithValue("comentario", obj.comentarios);
                    cmd.Parameters.AddWithValue("origen", obj.origen);
                    cmd.Parameters.AddWithValue("destino", obj.destino);
                    cmd.Parameters.AddWithValue("estatus", obj.estatus);
                    cmd.Parameters.AddWithValue("importe", obj.importe);
                    cmd.Parameters.AddWithValue("eliminar", obj.eliminar);
                    cmd.Parameters.Add("resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();
                    idautogenerado = Convert.ToInt32(cmd.Parameters["resultado"].Value);
                    Mensaje = cmd.Parameters["mensaje"].Value.ToString();



                }


            }
            catch (Exception e)
            {
                idautogenerado = 0;
                Mensaje = e.Message;

            }
            return idautogenerado;

        }
    }
}

