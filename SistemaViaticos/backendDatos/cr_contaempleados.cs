﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CapaEntidad;
using System.Data.SqlClient;
using System.Data;

namespace backendDatos
{
   public  class cr_contaempleados
    {
        public List<contableempleado> listarcontableempleado(int id)
        {
            List<contableempleado> lista = new List<contableempleado>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    StringBuilder sw = new StringBuilder();
                    sw.AppendLine("select co.id,cc.nombre,cc.descripcion,cu.nombre[nombreusuario],co.estatus,co.monto from cuentas_contables cc");
                    sw.AppendLine("inner join contablesempleado co on cc.id=co.clavecontable inner join usuarios cu on cu.id=co.claveusuario where co.clavesolicitud=@id");                    
                    SqlCommand cmd = new SqlCommand(sw.ToString(), oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista.Add(new contableempleado()
                            {
                                id = Convert.ToInt16(dr["id"]),
                                clavecontable = new cuentas_contables() { nombre = dr["nombre"].ToString(), descripcion=dr["descripcion"].ToString() },
                                claveusuario = new usuarios() { nombre = dr["nombreusuario"].ToString() },
                                estatus=dr["estatus"].ToString(),
                                monto =Convert.ToInt16( dr["monto"])
                              

                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<contableempleado>();

            }


            return lista;

        }

        public total listarconta(int id)
        {
            total lista = new total();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    String query = "select sum(monto) AS TOTAL from contablesempleado where clavesolicitud=@id";

                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;

                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista = new total()
                            {
                                totalcontable = Convert.ToDecimal(dr["TOTAL"])
                            };
                        }

                    }

                }
            }
            catch
            {
                lista = new total();

            }


            return lista;

        }
        public bool Elimarcontablempleado(int id, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("delete top (1) from contablesempleado where id=@id", oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;
                }
            }
            catch (Exception e)
            {
                resultado = false;
                Mensaje = e.Message;
            }
            return resultado;
        }
        public bool editarcontaempleados(contableempleado obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top(1) contablesempleado set monto ='{0}',clavecontable='{1}'  where id =@id", obj.monto,obj.clavecontable.id);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@monto", obj.monto);
                    cmd.Parameters.AddWithValue("@clavecontable", obj.clavecontable.id);


                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }

        public bool editarcomprobante(contableempleado obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top (1) contablesempleado set estatus='{0}' where id=@id", obj.estatus);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@importe", obj.estatus);

                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }


        public int registrarcontableempleado(contableempleado obj, out string Mensaje)
        {
            int idautogenerado = 0;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    // string query = String.Format("IF NOT EXISTS (SELECT * FROM cuentagastos where nombre='{0}')insert into cuentagastos(nombre) values('{0}')", obj.nombre);
                    SqlCommand cmd = new SqlCommand("cp_registrarcontaempleados", oconexion);
                    cmd.Parameters.AddWithValue("monto", obj.monto);
                    cmd.Parameters.AddWithValue("clavecontable", obj.clavecontable.id);
                    cmd.Parameters.AddWithValue("claveusuario", obj.claveusuario.id);
                    cmd.Parameters.AddWithValue("clavesolicitud", obj.clavesolicitud.idsolicitud);
                    cmd.Parameters.Add("Resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("Mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();
                    idautogenerado = Convert.ToInt32(cmd.Parameters["Resultado"].Value);
                    Mensaje = cmd.Parameters["Mensaje"].Value.ToString();

                }


            }
            catch (Exception e)

            {
                idautogenerado = 0;

                Mensaje = e.Message;

            }
            return idautogenerado
                ;
        }
    }
}

