﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data.SqlClient;
using System.Data;
namespace backendDatos
{
   public  class cr_proyecto
    {

        public List<proyecto> Listarproyecto()
        {
            List<proyecto> lista = new List<proyecto>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    String query = "select id, nombre,descripcion from proyecto";
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.CommandType = CommandType.Text;

                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista.Add(new proyecto()
                            {
                                id = Convert.ToInt16(dr["id"]),
                                nombre = dr["nombre"].ToString(),
                                descripcion = dr["descripcion"].ToString()
                              

                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<proyecto>();

            }


            return lista;

        }

        public int RegistrarProyecto(proyecto obj, out String Mensaje)
        {
            int idautogenerado = 0;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("cp_registraproyecto", oconexion);
                    cmd.Parameters.AddWithValue("nombre", obj.nombre);
                    cmd.Parameters.AddWithValue("descripcion", obj.descripcion);
                    cmd.Parameters.Add("Resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("Mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();
                    idautogenerado = Convert.ToInt32(cmd.Parameters["Resultado"].Value);
                    Mensaje = cmd.Parameters["Mensaje"].Value.ToString();

                }

            }
            catch (Exception e)
            {
                idautogenerado = 0;
                Mensaje = e.Message;

            }
            return idautogenerado;

        }

        public bool editarproyecto(proyecto obj, out String Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("cp_editarproyecto", oconexion);
                    cmd.Parameters.AddWithValue("id", obj.id);
                    cmd.Parameters.AddWithValue("nombre", obj.nombre);
                    cmd.Parameters.AddWithValue("descripcion", obj.descripcion);
                    cmd.Parameters.Add("Resultado", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("Mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();

                    resultado = Convert.ToBoolean(cmd.Parameters["Resultado"].Value);
                    Mensaje = cmd.Parameters["Mensaje"].Value.ToString();


                }


            }
            catch (Exception e)
            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;

        }
        public bool eliminarproyecto(int id, out String Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("delete top (1) from proyecto where id=@id", oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)
            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;

        }

    }
}
