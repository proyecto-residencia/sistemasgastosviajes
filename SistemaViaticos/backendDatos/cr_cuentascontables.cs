﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CapaEntidad;
using System.Data.SqlClient;
using System.Data;



namespace backendDatos
{
  public   class cr_cuentascontables
    {
        public List<cuentas_contables> listarcuentascontables()
        {            List<cuentas_contables> gasto = new List<cuentas_contables>();
            try            {
                using (SqlConnection conexionnueva = new SqlConnection(Conexion.con))
                {  StringBuilder sw = new StringBuilder();
                    sw.AppendLine("select cm.id,cm.nombre, cm.descripcion, c.id[idgastos],c.nombre[nombregasto]");
                    sw.AppendLine("from cuentagastos c");
                    sw.AppendLine("inner join cuentas_contables cm");
                    sw.AppendLine("on c.id = cm.clave_gasto");
                    SqlCommand cmd = new SqlCommand(sw.ToString(), conexionnueva);
                    cmd.CommandType = CommandType.Text;
                    conexionnueva.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {    while (dr.Read())
                        {
                            gasto.Add(new cuentas_contables()
                            {
                                id = Convert.ToInt16(dr["id"]),
                                nombre = dr["nombre"].ToString(),
                                descripcion = dr["descripcion"].ToString(),
                                ocuentagasto = new cuentagastos() {idgasto=Convert.ToInt16(dr["idgastos"]) ,nombre= dr["nombregasto"].ToString() } 
                            }
                         );
                        }
                    }
                }
            }
            catch (Exception e)
            {
                gasto = new List<cuentas_contables>();
            }
            return gasto;
        }
        public bool Elimarcuentascontables(int id, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("delete top (1) from cuentas_contables where id=@id", oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }
        public bool editarcuentascontables(cuentas_contables obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top(1) cuentas_contables set nombre ='{0}', descripcion='{1}', clave_gasto='{2}' where id =@id", obj.nombre,obj.descripcion,obj.ocuentagasto.idgasto);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@nombre", obj.nombre);
                    cmd.Parameters.AddWithValue("@descripcion", obj.descripcion);
                    cmd.Parameters.AddWithValue("@clave_gasto", obj.ocuentagasto.idgasto);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;
                }
            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }


        public int registrarcuentascontables(cuentas_contables obj, out string Mensaje)
        {
            int idautogenerado = 0;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    // string query = String.Format("IF NOT EXISTS (SELECT * FROM cuentagastos where nombre='{0}')insert into cuentagastos(nombre) values('{0}')", obj.nombre);
                    SqlCommand cmd = new SqlCommand("cp_registrarcuencontables", oconexion);
                    cmd.Parameters.AddWithValue("nombre", obj.nombre);
                    cmd.Parameters.AddWithValue("descripcion", obj.descripcion);
                    cmd.Parameters.AddWithValue("clave_gasto", obj.ocuentagasto.idgasto);
                    cmd.Parameters.Add("Resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("Mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();
                    idautogenerado = Convert.ToInt32(cmd.Parameters["Resultado"].Value);
                    Mensaje = cmd.Parameters["Mensaje"].Value.ToString();
                }
            }
            catch (Exception e)

            {
                idautogenerado = 0;
                Mensaje = e.Message;
            }
            return idautogenerado
                ;
        }
    }
}
