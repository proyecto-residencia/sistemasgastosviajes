﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaEntidad;

namespace SistemaGastoviajes.Models
{
    public class comprobante
    {


        public int id { get; set; }
        public solicitarviaje clavesolictud { get; set; }
        public cuentas_contables clavecuentagasto { get; set; }
        public decimal precio { get; set; }
        //  public String rutaimagen { get; set; }
        public String nombreimagen { get; set; }
        public String fechacaptura { get; set; }
        public String fechagasto { get; set; }
        public String comentario { get; set; }
        public String estatus { get; set; }
        public String Base64 { get; set; }
        public byte[] imagen { get; set; }


        public String Extension { get; set; }
    }
}