﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaEmpleado;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;


namespace SistemaGastoviajes.Controllers
{
    [Authorize]
    public class CatalogoController : Controller
    {
        // GET: Catalogo
        public ActionResult EstatusCom()
        {

            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);

            List<comprobante_gasto> olista = new List<comprobante_gasto>();
            olista = new Cn_comprobantegasto().Listacomprobante(ido);
             
            return View(olista);
        }

        public ActionResult EstatusViajes()
        {
            return View();
        }
        public ActionResult Proyectos()
        {
            return View();
        }
        public ActionResult CuentasContables()
        {
            return View();
        }
        public ActionResult UnidadAdmin()
        {
            return View();
        }
        public ActionResult CentroCostos()
        {
            return View();
        }
        public ActionResult Tipogastos()
        {
            return View();
        }

        public ActionResult Miscuentas()
        {
            return View();
        }





        public ActionResult formapago()
        {
            return View();
        }


        
        [HttpPost]



        public ActionResult Misvia(int id)
        {

            Session["idcuen"] = id;

          //  idm = id;

            return Json(new { data = id }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]



        public ActionResult ImporteViaje(int id)
        {

            Session["importe"] = id;

            //  idm = id;

            return Json(new { data = id }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public FileResult descargararchivo() {

            int archi = Convert.ToInt16( Session["idcuen"]);

             //   ido = Convert.ToInt16(Session["idcuen"]);


            comprobante_gasto co = new Cn_comprobantegasto().Listarimagen().Where(p => p.id == archi).FirstOrDefault(); ;


            String nombrecompleto = co.nombreimagen + co.Extension;



            return File(co.imagen,"application/" + co.Extension.Replace(".",""),nombrecompleto);

            } 
       

        [HttpGet]

        public JsonResult listacomdegasto()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);

            List<comprobante_gasto> olista = new List<comprobante_gasto>();
            olista = new Cn_comprobantegasto().Listacomprobante(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public JsonResult listacontaemple()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);

            List<contableempleado> olista = new List<contableempleado>();
            olista = new cn_contableempleado().Listarcontaemple(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]

        public JsonResult listarviaje()
        {
            List<solicitarviaje> olista = new List<solicitarviaje>();
            olista = new cn_solictarviaje().Listar2();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult listacontables()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);


            total olista = new cn_contableempleado().Listitacontable(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult sumarcompro()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);


            total olista = new Cn_comprobantegasto().sumarcompro(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public JsonResult imagencomprobante(int id)
        {
            bool conversion = true;

            comprobante_gasto co = new Cn_comprobantegasto().Listarimagen().Where(p => p.id == id).FirstOrDefault();

            string textoBase64 = Cn_recursos.convertirBase64(co.imagen, out conversion);
           
            return Json(new
            {
                conversion = conversion,
                textoBase64 = textoBase64,
                extension = Path.GetExtension(co.nombreimagen)
            },
            JsonRequestBehavior.AllowGet
            );





        }


        [HttpPost]
        public JsonResult editavia(solicitarviaje objeto)

        {
            object resultado;
            string Mensaje = string.Empty;
            resultado = new cn_solictarviaje().editaviajesito(objeto, out Mensaje);

            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult editacom(comprobante_gasto objeto)

        {


            object resultado;
            string Mensaje = string.Empty;

            resultado = new Cn_comprobantegasto().editarcomprobante(objeto, out Mensaje);



            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult editcomprobante(contableempleado objeto)

        {

            object resultado;
            string Mensaje = string.Empty;

            resultado = new cn_contableempleado().editarcomprobante(objeto, out Mensaje);



            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

        //proyecto......
        #region Proyecto

        [HttpGet]

        public JsonResult listaproyecto()
        {
            List<proyecto> olista = new List<proyecto>();
            olista = new Cn_proyecto().Listarproyecto2();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult guardarproyecto(proyecto objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.id == 0)
            {
                resultado = new Cn_proyecto().Registrarproyecto(objeto, out Mensaje);


            }
            else
            {
                resultado = new Cn_proyecto().editarproyecto(objeto, out Mensaje);

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult eliminarproyecto(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new Cn_proyecto().Elimarproyecto(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }

        #endregion


        // forma de pago 

        #region

        [HttpGet]

        public JsonResult listaformapago()
        {
            List<formapa> olista = new List<formapa>();
            olista = new cn_formapago().Listarformapago();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult guardarformago(formapa objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.id == 0)
            {
                resultado = new cn_formapago().Registrarformapago(objeto, out Mensaje);


            }
            else
            {
                resultado = new cn_formapago().editarformapago(objeto, out Mensaje);

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult eliminarformapago(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new cn_formapago().Elimarformapago(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        // cuenta centrogastos 

        #region

        [HttpGet]

        public JsonResult listacentrogasto()
        {
            List<centrodecostos> olista = new List<centrodecostos>();
            olista = new Cn_centrocostos().Listarcentrocostos();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult guardarcentrocosto(centrodecostos objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.id == 0)
            {
                resultado = new Cn_centrocostos().registrarcentrocosto(objeto, out Mensaje);


            }
            else
            {
                resultado = new Cn_centrocostos().editarcentrocosto(objeto, out Mensaje);

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult eliminarcentrocosto(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new Cn_centrocostos().Elimarcentrocosto(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }

        #endregion

        //cuenta gastos
        #region

        [HttpGet]

        public JsonResult listagastos()
        {
            List<cuentagastos> olista = new List<cuentagastos>();
            olista = new Cn_cuentagasto().Listargastos();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult guardargastos(cuentagastos objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.idgasto == 0)
            {
                resultado = new Cn_cuentagasto().Registrargastos(objeto, out Mensaje);


            }
            else
            {
                resultado = new Cn_cuentagasto().editargasto(objeto, out Mensaje);

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult eliminargasto(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new Cn_cuentagasto().Elimargasto(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }

        #endregion

        //cuentascontables

        #region

        [HttpGet]

        public JsonResult listacuentascontables()
        {
            List<cuentas_contables> olista = new List<cuentas_contables>();
            olista = new Cn_cuentascontables().Listarcuentascontables();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult guardarcuentascontables(cuentas_contables objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.id == 0)

            {
                resultado = new Cn_cuentascontables().Registrarcontables(objeto, out Mensaje);


            }
            else
            {
                resultado = new Cn_cuentascontables().editarcuentascontables(objeto, out Mensaje);

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult eliminarcuentascontables(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new Cn_cuentascontables().Elimarcuentascontables(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }
        #endregion

        [HttpGet]

        public JsonResult listauniadmin()
        {
            List<uniAdmin> olista = new List<uniAdmin>();
            olista = new Cn_unidadmin().Listaruniadmin();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult guardaruniadmin(uniAdmin objeto)
        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.id == 0)
            {
                resultado = new Cn_unidadmin().Registrarunidadadmin(objeto, out Mensaje);


            }
            else
            {
                resultado = new Cn_unidadmin().editargasto(objeto, out Mensaje);

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult eliminarunidad(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new Cn_unidadmin().Elimaruniadmino(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }


    }
}
