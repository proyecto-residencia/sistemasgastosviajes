﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaEmpleado;

namespace SistemaGastoviajes.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
      

        public ActionResult Usuarios()
        {
            return View();
        }
       
        [HttpGet]

        public JsonResult listausuario()
        {
            List<usuarios> olista = new List<usuarios>();
            olista = new CA_usuario().Listarusua();  
            return Json(new {data=olista },  JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult guardarusuario(usuarios objeto )

        {
           
            object resultado;
            string Mensaje = string.Empty;
            if (objeto.id == 0)
            {
               resultado = new CA_usuario().Registrar(objeto, out Mensaje);
              
            
            }
            else
            {
                resultado = new CA_usuario().editarusuario(objeto, out Mensaje);




            }
            return Json(new { resultado=resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult eliminarusuario(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new CA_usuario().Elimarusuario(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }



    }
    }