﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using backendDatos;
using CapaEntidad;

namespace CapaEmpleado
{
    public class CA_usuario
    {
        private cr_usuarios objbackenddatos = new cr_usuarios();

        public List<usuarios> Listarusua()
        {

            return objbackenddatos.ListarUsuario();
        }

        public int Registrar(usuarios obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre del usuario es vacio";
                    
            }else 
            if (string.IsNullOrEmpty(obj.usuario) || string.IsNullOrWhiteSpace(obj.usuario))
            {
                Mensaje = "el usuario del usuario es vacio";

            }else 
            if (string.IsNullOrEmpty(obj.email) || string.IsNullOrWhiteSpace(obj.email))
            {
                Mensaje = "el correo del usuario es vacio";

            }else
           
            if (string.IsNullOrEmpty(obj.sexo) || string.IsNullOrWhiteSpace(obj.sexo))
            {
                Mensaje = "del usuario es vacio";

            }
            else if (string.IsNullOrEmpty(obj.role) || string.IsNullOrWhiteSpace(obj.role))
                {
                Mensaje = "el rol del usuario es vacio";
            }
             
              if (string.IsNullOrEmpty(Mensaje)){

                string clave = Cn_recursos.GenerarClave();
                String asunto = "Creacion de cuenta";
                string mensaje_correo = "<h3>Su cuenta fue creada correctamente</h3></br><p>su contraseña es: ¡clave!</p>";
                mensaje_correo = mensaje_correo.Replace("¡clave!", clave);
                bool respuesta = Cn_recursos.enviarcorreo(obj.email, asunto, mensaje_correo);
                if (respuesta)
                {
                    //Cn_recursos.ConverIncriptar//(clave);
                    obj.contraseña = clave;
                    obj.restablecer = 1;
                    string rol = "empleado";
                    

                    obj.role = rol;
                    return objbackenddatos.Registrar(obj, out Mensaje);
                }
                else
                {
                    Mensaje = "no se pudo enviar el correo";
                    return 0;

                }


                
            }else
            {
                return 0;
            }

            
        }

        public bool editarusuario(usuarios obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre del usuario es vacio";

            }
            else
            if (string.IsNullOrEmpty(obj.usuario) || string.IsNullOrWhiteSpace(obj.usuario))
            {
                Mensaje = "el usuario del usuario es vacio";

            }
            else
            if (string.IsNullOrEmpty(obj.email) || string.IsNullOrWhiteSpace(obj.email))
            {
                Mensaje = "el correo del usuario es vacio";

            }
           
            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editarusuario(obj, out Mensaje);
            }
            else
            {
                return false;
            }




            }

        public bool Elimarusuario(int id, out string Mensaje)
        {
            return objbackenddatos.Elimarusuario(id, out Mensaje);
        }
        public bool cambiarclave (int idusuario, string nuevaclave, out string Mensaje)
        {
            return objbackenddatos.Cambiarclave(idusuario, nuevaclave, out Mensaje);
        }
        public bool Restablecerclave(int idusuario,string correo, out String Mensaje)
        {
            Mensaje = string.Empty;
            string nuevaclave = Cn_recursos.GenerarClave();
            bool resultado = objbackenddatos.restablecercontraseña(idusuario, nuevaclave,out Mensaje);
            if (resultado)
            {
                String asunto = "Contraseña restablecida";
                string mensaje_correo = "<h3>Su cuenta fue restablecida correctamente</h3></br><p>su contraseña para acceder ahora es: ¡clave!</p>";
                mensaje_correo = mensaje_correo.Replace("¡clave!", nuevaclave);
                bool respuesta = Cn_recursos.enviarcorreo(correo, asunto, mensaje_correo);

                if (respuesta)
                {
                    return true;

                }
                else
                {
                    Mensaje = "no se pudo enviar el correo";
                    return false;

                }

            }
            else
            {
                Mensaje = "no se restablecer la contraseña";

                return false;
            }

           

        }



    }
}
