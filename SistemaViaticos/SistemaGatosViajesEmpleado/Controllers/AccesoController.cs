﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CapaEmpleado;
using CapaEntidad;

namespace SistemaGatosViajesEmpleado.Controllers
{
    public class AccesoController : Controller
    {
        // GET: Acceso
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CambiarClave()
        {
            return View();
        }
        public ActionResult Restablecer()
        {
            return View();
        }

        public ActionResult Registrar()

        {

            //   string Mensaje = string.Empty;
            // Object[] datos =new Object[5];
            //  datos[0] = nombre;
            //  datos[1] = usuario;
            //  datos[2] = email;
            //  datos[3] = fecha;
            //  datos[4] = sexo;
            //  object resultado;
            // resultado = new CA_usuario().Registrar(datos, out Mensaje);

            return View();
        }

        [HttpPost]
        public JsonResult guardarusuario(usuarios objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            resultado = new CA_usuario().Registrar(objeto, out Mensaje);
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult Index(string correo, string clave)
        {
            usuarios cn = new usuarios();
            cn = new CA_usuario().Listarusua().Where(u => u.email == correo && u.contraseña == clave).FirstOrDefault();

            if (cn == null)
            {
                ViewBag.Error = "Correo contraseña incorrecta";
                return View();
            }
            else
            {
                if (cn.restablecer == 1)
                {
                    TempData["Id"] = cn.id;

                    return RedirectToAction("CambiarClave");

                }

                if (cn.role == "admin")
                {
                    ViewBag.Error = "El administrador no puede ingresar";
                    return View();




                }
               

                Session["cliente"] = cn.role;
                Session["idusuario"] = cn.id;
                Session["nombre"] = cn.nombre;

                FormsAuthentication.SetAuthCookie(cn.email, false);



                ViewBag.Error = null;
                return RedirectToAction("SolictarViaje", "Home");





            }



        }
        [HttpPost]
        public ActionResult CambiarClave(string id, string claveactual, string nuevaclave, string confirmarclave)
        {
            usuarios cn = new usuarios();
            cn = new CA_usuario().Listarusua().Where(u => u.id == int.Parse(id)).FirstOrDefault();
            if (cn.contraseña != claveactual)
            {
                TempData["Id"] = id;
                ViewBag.Error = "La contraseña actual no es correcta";
                return View();
            }
            else if (nuevaclave != confirmarclave)
            {
                TempData["Id"] = id;
                ViewData["claveusuario"] = claveactual;
                ViewBag.Error = "Las contraseñas no coinciden";
                return View();
            }
            ViewData["claveusuario"] = "";
            string mensaje = string.Empty;
            bool respuesta = new CA_usuario().cambiarclave(int.Parse(id), nuevaclave, out mensaje);
            if (respuesta)
            {
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Id"] = id;
                ViewBag.Error = mensaje;
                return View();

            }

        }
        [HttpPost]

        public ActionResult Restablecer(string correo)
        {
            usuarios ousuario = new usuarios();
            ousuario = new CA_usuario().Listarusua().Where(item => item.email == correo).FirstOrDefault();
            if (ousuario == null)
            {
                ViewBag.Error = "No se encontro dicho usuario";
                return View();
            }

            string mensje = string.Empty;
            bool respuesta = new CA_usuario().Restablecerclave(ousuario.id, correo, out mensje);

            if (respuesta)
            {
                ViewBag.Error = null;
                return RedirectToAction("Index", "Acceso");
            }
            else
            {
                ViewBag.Error = mensje;
                return View();
            }

        }
        public ActionResult cerrarsesion()
        {
            Session["cliente"] = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Acceso");
        }


    }
}