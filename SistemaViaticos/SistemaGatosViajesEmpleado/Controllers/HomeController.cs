﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaEmpleado;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using System.Web.Helpers;
using System.Drawing;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Kernel.Colors;
using iText.Layout.Properties;
using iText.Layout.Borders;
using iText.Kernel.Events;
using System.Runtime.Remoting.Messaging;
using System.Web.UI.WebControls;
using iText.IO.Image;
using Image = iText.Layout.Element.Image;
using Style = iText.Layout.Style;
using Table = iText.Layout.Element.Table;
using iText.Kernel.Geom;
using System.Web.Razor.Generator;
using iText.Kernel.Font;
using iText.IO.Font.Constants;
using System.Data;

namespace SistemaGatosViajesEmpleado.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        public int idm;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult imprimirpdf()
        {
            contableempleado gast = new contableempleado();
            MemoryStream ms = new MemoryStream();

            PdfWriter pw = new PdfWriter(ms);
            PdfDocument pdfDocument = new PdfDocument(pw);

          Document doc = new Document(pdfDocument);

            doc.SetMargins(75, 35, 70, 35);
            string pathLogo = Server.MapPath("~/Content/image/tangente.png");


           Image img = new Image(ImageDataFactory.Create(pathLogo)); 

           pdfDocument.AddEventHandler(PdfDocumentEvent.START_PAGE, new HeaderHandler2(img));

            doc.Add(new iText.Layout.Element.Paragraph("reporte de gastos de viajes").SetFontSize(14)
                .SetTextAlignment(TextAlignment.CENTER));
                

            Style styli = new Style().SetBackgroundColor(ColorConstants.LIGHT_GRAY)
                .SetTextAlignment(TextAlignment.CENTER);
            //    .SetBorder(Border.NO_BORDER);



            Table _tabla2 = new Table(6).UseAllAvailableWidth();
            Cell _cell2 = new Cell(2, 1).Add(new iText.Layout.Element.Paragraph("EMPLEADO"));
            _tabla2.AddHeaderCell(_cell2.AddStyle(styli));
            _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph("REFVIAJE"));
            _tabla2.AddHeaderCell(_cell2.AddStyle(styli));
            _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph("FECHACAPTURA"));
            _tabla2.AddHeaderCell(_cell2.AddStyle(styli));
            _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph("CENTROCOSTO"));
            _tabla2.AddHeaderCell(_cell2.AddStyle(styli));
            _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph("ORIGEN"));
            _tabla2.AddHeaderCell(_cell2.AddStyle(styli));
            _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph("DESTINO"));
            _tabla2.AddHeaderCell(_cell2);

            int ido3;

            ido3 = Convert.ToInt16(Session["idcuen"]);

            List<solicitarviaje> lista = new List<solicitarviaje>();
            lista = new cn_solictarviaje().Listarporviaje(ido3);



            foreach (var item in lista)
            {

                _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph(item.idsolicitud.ToString()));
                _tabla2.AddCell(_cell2.SetBackgroundColor(ColorConstants.RED));
                _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph(item.nombreviatico));
                _tabla2.AddCell(_cell2.SetBackgroundColor(ColorConstants.ORANGE));
                _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph(item.fechacaptura));
                _tabla2.AddCell(_cell2.SetBackgroundColor(ColorConstants.YELLOW));
                _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph(item.centrocosto.nombre.ToString()));
                _tabla2.AddCell(_cell2.SetBackgroundColor(ColorConstants.BLUE));
                _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph(item.origen));
                _tabla2.AddCell(_cell2.SetBackgroundColor(ColorConstants.GREEN));

                _cell2 = new Cell().Add(new iText.Layout.Element.Paragraph(item.destino));
                _tabla2.AddCell(_cell2.SetBackgroundColor(ColorConstants.GRAY));

            }

            doc.Add(_tabla2);



            Table _tabla = new Table(5).UseAllAvailableWidth();
            Cell _cell = new Cell(2, 1).Add(new iText.Layout.Element.Paragraph("ID"));
            _tabla.AddHeaderCell(_cell.AddStyle(styli));
            _cell = new Cell().Add(new iText.Layout.Element.Paragraph("CONCEPTO"));
            _tabla.AddHeaderCell(_cell.AddStyle(styli));
            _cell = new Cell().Add(new iText.Layout.Element.Paragraph("DESCRIPCION"));
            _tabla.AddHeaderCell(_cell.AddStyle(styli));
            _cell = new Cell().Add(new iText.Layout.Element.Paragraph("EMPLEADO"));
            _tabla.AddHeaderCell(_cell.AddStyle(styli));
            _cell = new Cell().Add(new iText.Layout.Element.Paragraph("IMPORTE"));
            _tabla.AddHeaderCell(_cell.AddStyle(styli));
           


            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);

            List<contableempleado> olista = new List<contableempleado>();
            olista = new cn_contableempleado().Listarcontaemple(ido);
            

            
            foreach (var item in olista)
            {
                
                _cell = new Cell().Add(new iText.Layout.Element.Paragraph(item.id.ToString()));
                _tabla.AddCell(_cell.SetBackgroundColor(ColorConstants.RED));
                _cell = new Cell().Add(new iText.Layout.Element.Paragraph(item.clavecontable.nombre));
                _tabla.AddCell(_cell.SetBackgroundColor(ColorConstants.ORANGE));
                _cell = new Cell().Add(new iText.Layout.Element.Paragraph(item.clavecontable.descripcion));
                _tabla.AddCell(_cell.SetBackgroundColor(ColorConstants.YELLOW));
                _cell = new Cell().Add(new iText.Layout.Element.Paragraph(item.claveusuario.nombre));
                _tabla.AddCell(_cell.SetBackgroundColor(ColorConstants.BLUE));
                _cell = new Cell().Add(new iText.Layout.Element.Paragraph(item.monto.ToString()));
                _tabla.AddCell(_cell.SetBackgroundColor(ColorConstants.GREEN));

            }

            doc.Add(_tabla);

         

            int ido2;

            ido2 = Convert.ToInt16(Session["idcuen"]);
            

            total olista2 = new cn_contableempleado().Listitacontable(ido2);


            doc.Add(new iText.Layout.Element.Paragraph("total").SetFontSize(14));

          doc.Add(new iText.Layout.Element.Paragraph(olista2.totalcontable.ToString()).SetFontSize(14));
            doc.Close();

            byte[] bytesStream = ms.ToArray();
            ms = new MemoryStream();
            ms.Write(bytesStream, 0, bytesStream.Length);

            ms.Position = 0;



            return new FileStreamResult(ms,"application/pdf");
        }

        public class HeaderHandler2 : IEventHandler
        {

            Image Img;
            public HeaderHandler2(Image img)
            {
                Img = img;
            }
            public void HandleEvent(Event @event)
            {
                PdfDocumentEvent docEvent = (PdfDocumentEvent)@event;
                PdfDocument pdfdoc = docEvent.GetDocument();
                PdfPage page = docEvent.GetPage();

                iText.Kernel.Geom.Rectangle roota = new iText.Kernel.Geom.Rectangle(35,page.GetPageSize().GetTop() - 70, page.GetPageSize().GetRight() - 70, 50);
                Canvas canvas = new Canvas(page, roota);
                canvas
                .Add(getTabla(docEvent))
                 
                 
                 .Close();






            }


            public Table getTabla(PdfDocumentEvent docEvent)
            {
                float[] cellwidth = { 20f, 80f };
                Table tableEvent = new Table(UnitValue.CreatePercentArray(cellwidth)).UseAllAvailableWidth();

                Style styleCell = new Style()
                    .SetBorder(Border.NO_BORDER);

                Style styleText = new Style()
                    .SetTextAlignment(TextAlignment.RIGHT).SetFontSize(10f);

                Cell cell = new Cell().Add(Img.SetAutoScale(true)).SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                tableEvent.AddCell(cell
                    .AddStyle(styleCell)
                    .SetTextAlignment(TextAlignment.LEFT));
                PdfFont bold = PdfFontFactory.CreateFont(StandardFonts.TIMES_BOLD);
                cell = new Cell()
                    .Add(new iText.Layout.Element.Paragraph("Reporte de gastos de viajes\n").SetFont(bold))
                    .Add(new iText.Layout.Element.Paragraph("Reporte de empleados\n").SetFont(bold))
                    .Add(new iText.Layout.Element.Paragraph("fecha de emision:" + DateTime.Now.ToShortDateString()))
                    .AddStyle(styleText).AddStyle(styleCell)
                    .SetBorder(new SolidBorder(ColorConstants.BLACK, 1));

                tableEvent.AddCell(cell);


                return tableEvent;

            }
        }


        public ActionResult Misviajes()
            
        {
            
            
            return View();
        }
        public ActionResult Imprimir()
        {
            return new Rotativa.ActionAsPdf("SolictarViaje");
        }
        public ActionResult MisComprobantes()

        {


            return View();
        }

        public ActionResult AgregarComprobante()

        {


            return View();

        }
        [HttpPost]
        
        public ActionResult Misvia(int id)
        {
            
            Session["idcuen"]=id;

            idm = id;

            return Json(new { data = id }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]

        public ActionResult misfechas(string fecha1,string fecha2)
        {

            Session["fec"] = fecha1;
            Session["fech"] = fecha2;

            

            return Json(new { data = fecha1,fecha2 }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SolictarViaje()
        {


            return View();
        }
        [HttpGet]

        public JsonResult listausuario()
        {
            List<usuarios> olista = new List<usuarios>();
            olista = new CA_usuario().Listarusua();
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public JsonResult listaproyecto()
        {
            List<proyecto> olista = new List<proyecto>();
            olista = new Cn_proyecto().Listarproyecto2();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]

        public JsonResult listauniadmin()
        {
            List<uniAdmin> olista = new List<uniAdmin>();
            olista = new Cn_unidadmin().Listaruniadmin();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]

        public JsonResult listacentrogasto()
        {
            List<centrodecostos> olista = new List<centrodecostos>();
            olista = new Cn_centrocostos().Listarcentrocostos();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        

        [HttpGet]

        public JsonResult listarviaje()
        {
            List<solicitarviaje> olista = new List<solicitarviaje>();
            olista = new cn_solictarviaje().Listar2();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public JsonResult listandofiltro()
        {
            int id = Convert.ToInt32(Session["idusuario"]);
            string fechainical = Convert.ToString(Session["fec"]);
            string fechafinal = Convert.ToString(Session["fech"]); ;
                List<solicitarviaje> olista = new List<solicitarviaje>();
            olista = new cn_solictarviaje().Listarporfecha(id, fechainical, fechafinal);


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public JsonResult listarviatico()
        {
            int id = Convert.ToInt32(Session["idcuen"]);
            
            List<solicitarviaje> olista = new List<solicitarviaje>();
            olista = new cn_solictarviaje().Listarporviaje(id);


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public JsonResult listarviajeus()
        {
            int id = Convert.ToInt32(Session["idusuario"]);
            List<solicitarviaje> olista = new List<solicitarviaje>();
            olista = new cn_solictarviaje().Listarporusua(id);


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]



        public ActionResult ImporteViaje(int id)
        {

            Session["importe"] = id;

            //  idm = id;

            return Json(new { data = id }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult sumarcompro()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);


            total olista = new Cn_comprobantegasto().sumarcompro(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult guardarviajes(solicitarviaje objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.idsolicitud == 0)

            {
                resultado = new cn_solictarviaje().Registrar(objeto, out Mensaje);  
                    
            }
            else
            {
                resultado = new cn_solictarviaje().editarsolicitarviajes(objeto, out Mensaje);

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public FileResult descargararchivo()
        {

            int archi = Convert.ToInt16(Session["idcuen"]);

            //   ido = Convert.ToInt16(Session["idcuen"]);


            comprobante_gasto co = new Cn_comprobantegasto().Listarimagen().Where(p => p.id == archi).FirstOrDefault(); ;


            String nombrecompleto = co.nombreimagen + co.Extension;



            return File(co.imagen, "application/" + co.Extension.Replace(".", ""), nombrecompleto);

        }




        [HttpPost]
        public JsonResult guardarcontableempleado(contableempleado objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.id== 0)

            {
                resultado = new cn_contableempleado().Registrarcontableempleado(objeto, out Mensaje);


            }
            else
            {
                resultado = false;

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult editandoimporte(solicitarviaje objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
           
                resultado = new cn_solictarviaje().editaimpo(objeto, out Mensaje);


          
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult bajaviajes(solicitarviaje objeto)

        {

            object resultado;
            string Mensaje = string.Empty;

            resultado = new cn_solictarviaje().bajaviaje(objeto, out Mensaje);



            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]

        public JsonResult listacuentascontables()
        {

            List<cuentas_contables> olista = new List<cuentas_contables>();
            olista = new Cn_cuentascontables().Listarcuentascontables();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        #region

        [HttpGet]

        public JsonResult listacontaemple()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);

            List<contableempleado> olista = new List<contableempleado>();
            olista = new cn_contableempleado().Listarcontaemple(ido);

            
            
            return Json(new { data = olista  }, JsonRequestBehavior.AllowGet);

            
        }

        [HttpGet]

        public JsonResult listacontables()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);

            
         total olista = new cn_contableempleado().Listitacontable(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult editacontable(contableempleado objeto)
        {

            object resultado;
            string Mensaje = string.Empty;
          
                resultado = new cn_contableempleado().editarcuentascontables(objeto, out Mensaje);

            
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }



        #endregion
        [HttpPost]
        public JsonResult eliminarcoempleado(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new cn_contableempleado().Elimarcuentascontables(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }

        //comprobantegasto

        [HttpGet]

        public JsonResult listacomdegasto()
        {
            int ido;

          ido= Convert.ToInt16(Session["idcuen"]);

            List<comprobante_gasto> olista = new List<comprobante_gasto>();
            olista = new Cn_comprobantegasto().Listacomprobante(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult guardarcomprobanti(string  objeto, HttpPostedFileBase archivoimagen)

        {
            

            
            string Mensaje = string.Empty;
            bool operacionexitosa = true;
            bool guardarimagenexito = true;

            comprobante_gasto com = new comprobante_gasto();
            com = JsonConvert.DeserializeObject<comprobante_gasto>(objeto);

             




            if (com.id == 0)

            {
                int idgenerado = new Cn_comprobantegasto().Registrarcomgastos(com, out Mensaje);
                if (idgenerado != 0)
                {
                    com.id = idgenerado;
                }
                else
                {
                    operacionexitosa = false;
                }

            }
            else
            {
                operacionexitosa = new Cn_comprobantegasto().editarcomprobantegasto(com, out Mensaje);

            }

            if (operacionexitosa)
            {
                if (archivoimagen != null)
                {
              //      string rutaimagen = ConfigurationManager.AppSettings["servidordefotos"];
                    string extension = System.IO.Path.GetExtension(archivoimagen.FileName);
                    string nombre_imagen = string.Concat(com.id.ToString());

           //         try
          //          {
            //            archivoimagen.SaveAs(Path.Combine(rutaimagen, nombre_imagen));
              //      }catch (Exception ex)
            //        {
              //          string msg = ex.Message;
                //        guardarimagenexito = false;
                  //  }

                    if (guardarimagenexito)
                    {
                       bool conversion;
                     //    com.rutaimagen = rutaimagen;
                        com.nombreimagen = nombre_imagen;
                        com.Extension = extension;

                        MemoryStream imag = new MemoryStream();
                        archivoimagen.InputStream.CopyTo(imag);

                        byte[] im = imag.ToArray();

                      //  Bitmap imagencompro = new Bitmap(archivoimagen.InputStream);

                       string textoBase64 = Cn_recursos.convertirBase64(im, out conversion);

                        //  "data:image/jpg;base64," + 
                        //

                        com.imagen = im;


                        bool res = new Cn_comprobantegasto().guardardatosimagen(com, out Mensaje);
                    }
                    else
                    {
                        Mensaje = "hubo problemas con la imagen";
                    }
                }
            }


            return Json(new { operacionexitosa = operacionexitosa,idgenerado=com.id, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

      

        [HttpPost]

        public JsonResult imagencomprobante(int id)
        {
            bool conversion= true;
            

            comprobante_gasto co = new Cn_comprobantegasto().Listarimagen().Where(p => p.id == id).FirstOrDefault();

            


         string textoBase64 = Cn_recursos.convertirBase64(co.imagen, out conversion);
         //  string textoBase64 = Cn_recursos.convertirBase64(Path.Combine(co.rutaimagen, co.nombreimagen), out conversion);


            return Json(new
            {
               conversion = conversion,
               textoBase64 = textoBase64,
               extension = System.IO.Path.GetExtension(co.nombreimagen)
            },
            JsonRequestBehavior.AllowGet
            );





        }

        [HttpPost]
        public JsonResult eliminarcom(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new Cn_comprobantegasto().Elimarcomprobantegas(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }



    }
}